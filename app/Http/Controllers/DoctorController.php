<?php

namespace App\Http\Controllers;

use App\Doctor;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    public function index () {
        $doctor = Doctor::all();
        return view('dr_profile', compact('doctor'));
    }
}
