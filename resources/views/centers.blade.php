@extends('layouts.app')


@section('content')


    <!-- bar -->
    <div class="intro intro-small" data-stellar-background-ratio="0" style="background-image: url('#'); background-position: 0px 0px;">
        <div class="container">
            <div class="row">
                <div class="intro-caption">
                    <h5>شـــاهد </h5>

                    <h2>المراكز الطبية</h2>
                </div>
                <!-- /.row -->
            </div>
        </div>
    </div>
    <!-- end bar -->
    <!-- centers -->
    <section class="centers">
        <div class="container">
            <div class="row">
               @for($i = 0; $i < 6; $i++)

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="service">
                            <div class="service-box">
                                <div class="service-image">
                                    <a href="#">
                                        <img src="{{asset('images/temp/service2.jpg')}}" item-prop="image" alt="">
                                    </a>
                                </div>
                                <!-- /.service-image -->

                                <div class="service-body">
                                    <h6>مركز الشفاء الطبي</h6>

                                    <p>مركز الشفاء الطبي من اكبر مراكز القاهرةالطبية يوجد به جميع التخصصات ويوجد به فريق طبي
                                        ممتاز على اعلي مستوي</p>

                                    <a href="{{route('center_profile')}}" class="link-more" item-prop="url">
                                        <i class="fa fa-plus"></i>
                                        معلومات اكثر
                                    </a>
                                </div>
                                <!-- /.service-body -->
                            </div>
                            <!-- /.service-box -->
                        </div>
                    </div>
                @endfor


            </div>
        </div>

        <div class="numbers">
            <div class="container">
                <div class="row">
                    <div class="paging">
                        <ul>
                            <li class="current">
                                <a href="#">1</a>
                            </li>

                            <li>
                                <a href="#">2</a>
                            </li>

                            <li>
                                <a href="#">3</a>
                            </li>

                            <li>
                                <a href="#">4</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.paging -->
                </div>
            </div>
        </div>
    </section>
    <!--end centers -->
    @endsection
