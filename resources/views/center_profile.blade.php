@extends('layouts.app')

@section('content')

    <!-- bar -->
    <div class="intro intro-small" data-stellar-background-ratio="0" style="background-image: url('#'); background-position: 0 0;">
        <div class="container">
            <div class="row">
                <div class="intro-caption">
                    <h5>مركز</h5>

                    <h2>الشفاء الطبي</h2>
                </div>
                <!-- /.row -->
            </div>
        </div>
    </div>
    <!-- end bar -->
    <!-- contain -->
    <section class="contain  ">
        <div class="container">
            <div class="row panel">
                <div class="col-md-5 col-xs-12 p-0" style="padding-right:0">
                    <img src="{{asset('images/temp/service2.jpg')}}" alt="" class="img-responsive about-centers">
                </div>
                <div class="col-md-7 col-xs-12">
                    <div class="about-center ">
                        <p>مركز الشفاء من اكبر مراكز الطبية فى المدينة لدية افضل الأطباء و يقدم الرعاية الافضل للمرضي ويوجد
                            لديه جميع التخصصات </p>
                        <ul class="center-info">
                            <li>
                                <span>العنوان : </span> شارع جيهان , المنصورة ,الدقهلية .مصر</li>
                            <li>
                                <span>التلفون :</span> 010234564664</li>
                            <li>
                                <span>الفاكس :</span> 2156486</li>
                            <li>
                                <span>البريد الألكتروني :</span> example@example.com</li>
                        </ul>
                        <div class="socials ">
                            <ul class="">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end contain -->
    <!-- tabs -->
    <section class="tabs">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#one">
                    <span class="icon">
                        <i class="icon-eye-sign"></i>
                    </span>
                    عيون
                </a>
            </li>
            <li>
                <a data-toggle="tab" href="#two">
                    <span class="icon">
                        <i class="icon-ear"></i>
                    </span>
                    انف و أذن</a>
            </li>
            <li>
                <a data-toggle="tab" href="#three">
                    <span class="icon">
                        <i class="icon-brain"></i>
                    </span>
                    مخ واعصاب</a>
            </li>
            <li>
                <a data-toggle="tab" href="#four">
                    <span class="icon">
                        <i class="icon-tooth"></i>
                    </span>
                    أسنان</a>
            </li>
            <li>
                <a data-toggle="tab" href="#five">
                    <span class="icon">
                        <i class="icon-heart-attack"></i>
                    </span>
                    باطنة</a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="one" class="tab-pane fade in active">
                <div class="container">
                    <div class="row">

                        @for($i = 0; $i < 12; $i++)
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="doctor">
                                    <h5 class="doctor-name">د/ محمد حسين</h5>
                                    <!-- /.doctor-name -->

                                    <div class="doctor-box">
                                        <div class="doctor-image">
                                            <a href="{{route('dr_profile')}}">
                                                <img src="{{asset('images/doctor and center-2.jpg')}}" alt="د/محمد حسين">
                                            </a>
                                        </div>


                                        <div class="doctor-body">
                                            <h6>رئيس القسم</h6>

                                            <div class="socials">
                                                <ul>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-google-plus"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.socials -->

                                            <p>دكتور متخصص فى جراحة القلب واستاذ دكتور بجامعة القاهرة ويعمل في مركز الشفاء الطبي</p>

                                            <a href="{{route('dr_profile')}}" class="link-more">
                                                <i class="fa fa-plus"></i>
                                                للمزيد
                                            </a>
                                        </div>
                                        <!-- /.doctor-body -->
                                    </div>
                                    <!-- /.doctor-box -->
                                </div>
                            </div>
                        @endfor

                    </div>
                </div>
            </div>
            <div id="two" class="tab-pane fade">
                <div class="container">
                    <div class="row">
                        @for($i = 0; $i < 12; $i++)
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="doctor">
                                    <h5 class="doctor-name">د/ محمد حسين</h5>
                                    <!-- /.doctor-name -->

                                    <div class="doctor-box">
                                        <div class="doctor-image">
                                            <a href="{{route('dr_profile')}}">
                                                <img src="{{asset('images/doctor and center-2.jpg')}}" alt="د/محمد حسين">
                                            </a>
                                        </div>


                                        <div class="doctor-body">
                                            <h6>رئيس القسم</h6>

                                            <div class="socials">
                                                <ul>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-google-plus"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.socials -->

                                            <p>دكتور متخصص فى جراحة القلب واستاذ دكتور بجامعة القاهرة ويعمل في مركز الشفاء الطبي</p>

                                            <a href="{{route('dr_profile')}}" class="link-more">
                                                <i class="fa fa-plus"></i>
                                                للمزيد
                                            </a>
                                        </div>
                                        <!-- /.doctor-body -->
                                    </div>
                                    <!-- /.doctor-box -->
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
            <div id="three" class="tab-pane fade">
                <div class="container">
                    <div class="row">
                        @for($i = 0; $i < 12; $i++)
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="doctor">
                                    <h5 class="doctor-name">د/ محمد حسين</h5>
                                    <!-- /.doctor-name -->

                                    <div class="doctor-box">
                                        <div class="doctor-image">
                                            <a href="{{route('dr_profile')}}">
                                                <img src="{{asset('images/doctor and center-2.jpg')}}" alt="د/محمد حسين">
                                            </a>
                                        </div>


                                        <div class="doctor-body">
                                            <h6>رئيس القسم</h6>

                                            <div class="socials">
                                                <ul>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-google-plus"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.socials -->

                                            <p>دكتور متخصص فى جراحة القلب واستاذ دكتور بجامعة القاهرة ويعمل في مركز الشفاء الطبي</p>

                                            <a href="{{route('dr_profile')}}" class="link-more">
                                                <i class="fa fa-plus"></i>
                                                للمزيد
                                            </a>
                                        </div>
                                        <!-- /.doctor-body -->
                                    </div>
                                    <!-- /.doctor-box -->
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
            <div id="four" class="tab-pane fade">
                <div class="container">
                    <div class="row">
                        @for($i = 0; $i < 12; $i++)
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="doctor">
                                    <h5 class="doctor-name">د/ محمد حسين</h5>
                                    <!-- /.doctor-name -->

                                    <div class="doctor-box">
                                        <div class="doctor-image">
                                            <a href="{{route('dr_profile')}}">
                                                <img src="{{asset('images/doctor and center-2.jpg')}}" alt="د/محمد حسين">
                                            </a>
                                        </div>


                                        <div class="doctor-body">
                                            <h6>رئيس القسم</h6>

                                            <div class="socials">
                                                <ul>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-google-plus"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.socials -->

                                            <p>دكتور متخصص فى جراحة القلب واستاذ دكتور بجامعة القاهرة ويعمل في مركز الشفاء الطبي</p>

                                            <a href="{{route('dr_profile')}}" class="link-more">
                                                <i class="fa fa-plus"></i>
                                                للمزيد
                                            </a>
                                        </div>
                                        <!-- /.doctor-body -->
                                    </div>
                                    <!-- /.doctor-box -->
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
            <div id="five" class="tab-pane fade">
                <div class="container">
                    <div class="row">
                        @for($i = 0; $i < 12; $i++)
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="doctor">
                                    <h5 class="doctor-name">د/ محمد حسين</h5>
                                    <!-- /.doctor-name -->

                                    <div class="doctor-box">
                                        <div class="doctor-image">
                                            <a href="{{route('dr_profile')}}">
                                                <img src="{{asset('images/doctor and center-2.jpg')}}" alt="د/محمد حسين">
                                            </a>
                                        </div>


                                        <div class="doctor-body">
                                            <h6>رئيس القسم</h6>

                                            <div class="socials">
                                                <ul>
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-google-plus"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.socials -->

                                            <p>دكتور متخصص فى جراحة القلب واستاذ دكتور بجامعة القاهرة ويعمل في مركز الشفاء الطبي</p>

                                            <a href="{{route('dr_profile')}}" class="link-more">
                                                <i class="fa fa-plus"></i>
                                                للمزيد
                                            </a>
                                        </div>
                                        <!-- /.doctor-body -->
                                    </div>
                                    <!-- /.doctor-box -->
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end tabs -->
    <!-- map  -->
    <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13676.636528013747!2d31.3941833!3d31.021815750000002!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sar!2seg!4v1552563986919"
            width="100%" height="450"  style="border:0" allowfullscreen></iframe>
    <!-- end map -->
    @endsection
