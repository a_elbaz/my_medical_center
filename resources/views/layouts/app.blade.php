<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My_Medical_Center</title>
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">
    <!--Default Theme-->
    <link rel="stylesheet" href="{{asset('css/owl.theme.css')}}">
    <link rel="shortcut icon" type="image/png" href="">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-rtl.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>

<body>
<!-- header -->
<a id="scroll-top" href="{{asset('#top')}}"><button>Top</button></a>
<header>
    <div class="container">
        <div class="header-bg">
            <div class="logo">
                <img src="{{asset('images/logo.png')}}" alt="logo">
            </div>
            <span class="fa fa-bars hidden-lg hidden-md open-nav"></span>
            <div class="nav-content">
                <nav class="clear fix">
                    <div class="nav-bar clear fix">
                        <span class="close-btn ex hidden-lg hidden-md">X</span>
                        <ul class="clear fix">
                            <li>
                                <a class="close-btn scroll-down" href="{{route('home')}}">الرئيسية</a>
                            </li>
                            <li>
                                <a class="close-btn scroll-down" href="{{route('center')}}">المراكز الطبية </a>
                            </li>
                            <li>
                                <a class="close-btn scroll-down" href="{{asset('#about')}}">عن الموقع</a>
                            </li>
                            <li>
                                <a class="close-btn scroll-down" href="{{asset('#contact')}}">تواصل معانا</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- end header -->
@yield('content')
<!-- footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="links">
                    <h4>
                        <i class="fa fa-link"></i>
                        روابط نصية
                    </h4>
                    <ul>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                        <li>
                            <a href="">رابط نصى</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="footer-form">
                    <h4>
                        <i class="fa fa-envelope"></i>
                        النشرة البريدية
                    </h4>
                    <p>اشترك بالنشره البريديه ليصلك جديد زفين </p>
                    <form action="">
                        <div class="form-group">
                            <input type="text" placeholder="ادخل بريدك الالكترونى هنا">
                        </div>
                        <button class="btn btn-danger">اشتراك</button>
                    </form>

                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="footer-logo">
                    <img class="img-responsive" src="{{asset('images/404-icon.png')}}" alt="logo">
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="bottom-footer clear fix">
    <div class="col-xs-12">
        <div class="left">
            <p>تصميم وتطوير </p>
        </div>
        <div class="right">
            <p>جميع الحقوق محفوظه لدي موقع طبي لسنة 2019</p>
        </div>
    </div>
</div>
<!-- end footer -->
<script src="{{asset('js/jquery-3.1.0.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.js')}}"></script>
<script src="{{asset('js/plugin.js')}}"></script>
<script src="{{asset('js/jquery.mixitup.js')}}"></script>
</body>

</html>
