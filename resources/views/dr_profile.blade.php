@extends('layouts.app')



@section('content')
    <!-- bar -->
    <div class="intro intro-small" data-stellar-background-ratio="0" style="background-image: url('#'); background-position: 0px 0px;">
        <div class="container">
            <div class="row">
                <div class="intro-caption">
                    <h5>الدكتور </h5>

                    <h2>محمد حسين</h2>
                </div>
                <!-- /.row -->
            </div>
        </div>
    </div>
    <!-- end bar -->
    <!-- centers -->
    <section class="dr-profile">
        <article class="event article-single-event">

            <div class="event-boxs">
                <div class="event-body" itemprop="articleBody">
                    <div class="doctor-profile">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <img src="images/doctor and center-2.jpg" alt="Doctor" class="img-responsive">
                                </div>

                                <div class="col-md-8 col-xs-12">
                                    <div class="bg-txt">
                                        <p>إستشارى الجراحة العامة و التجميل و الطوارئ و العناية المركزة دكتور جراحة عامة متخصص
                                            في جراحة عامة بالغين، جراحة اورام الثدي، جراحة بطن، جراحة اصابات وحوادث، جراحة
                                            أورام بالغين، جراحة اورام البروستاتا و جراحة مسالك بولية بالغين اقل</p>

                                        <p>إستشارى الجراحة العامة و التجميل و الطوارئ جميع حالات الجراحة العامة- عمليات الطهارة
                                            للاطفال-عمليات إزالة الظفر المنغرس-خياطة الجروح وغيارها بالمنزل-عمليات البواسير-استئصال
                                            الكيس الدهنى امكانية الكشف على حالات الطوارئ ٢٤ساعة</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dr-info">

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">

                                    <h3 class="txt-color">خبرة العمل</h3>

                                    <h4>مستشفي العام من 2014 حتي الآن</h4>

                                    <p>رئيس قسم الجراحة </p>
                                    <hr>
                                    <h4>مستشفي العام من 2014 حتي الآن</h4>
                                    <p>رئيس قسم الجراحة </p>
                                    <hr>
                                    <h4>مستشفي العام من 2014 حتي الآن</h4>
                                    <p>رئيس قسم الجراحة </p>
                                    <hr>
                                    <blockquote>
                                        استشاري الجراحة العامة والجهاز الهضمى والمناظير والسمنة وعضو كلية الجراحين الملكية لندن .جراحة عامة متخصص في جراحة عامة بالغين،
                                        جراحة اورام الثدي، جراحة بطن، جراحة غدد صماء، جراحة جهاز هضمي ومناظير بالغين، ومناظير
                                        اطفال، جراحة اصابات وحوادث، جراحة أورام بالغين، جراحة اورام القولون، جراحة اورام
                                        المعدة، جراحة عامة اطفال، جراحة سمنة وتخسيس.</blockquote>
                                </div>
                            </div>
                            <div class="row panel">
                                <div class="col-xs-12">
                                    <div class="doctor-program">
                                        <h4>أوقات العمل</h4>
                                        <ul class="small-block-grid-1 medium-block-grid-1 large-block-grid-2">
                                            <li>
                                                <span>09:00 - 13:00</span>
                                                السبت
                                            </li>
                                            <li>
                                                <span>13:00 - 17:00</span>
                                                الثلاثاء
                                            </li>
                                            <li>
                                                <span>09:00 - 13:00</span>
                                                الخميس
                                            </li>
                                            <li>
                                                <span>13:00 - 17:00</span>
                                                الجمعة
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="widget widget-form">
                                    <h4>سهل وسريع</h4>
                                    <h3>أحجز موعدك للكشف</h3>
                                    <div class="from-appointment">
                                        <form action="?" method="post" novalidate="novalidate">
                                            <div class="form-row">
                                                <div class="form-controls">
                                                    <input type="text" class="field" required="" name="field-fname2" id="field-fname2" value="" placeholder="الأسم بالكامل" aria-required="true">
                                                </div>
                                                <!-- /.form-controls -->
                                            </div>
                                            <!-- /.form-row -->

                                            <div class="form-row">
                                                <div class="form-controls">
                                                    <input type="email" class="field" required="" name="field-email2" id="field-email2" value="" placeholder="بريدك الألكتروني"
                                                           aria-required="true">
                                                </div>
                                                <!-- /.form-controls -->
                                            </div>
                                            <!-- /.form-row -->

                                            <div class="form-row">
                                                <div class="form-controls">
                                                    <input type="tel" class="field" required="" name="field-phone" id="field-phone" value="" placeholder="رقم التلفون" aria-required="true">
                                                </div>
                                                <!-- /.form-controls -->
                                            </div>
                                            <!-- /.form-row -->

                                            <div class="form-row">
                                                <div class="form-controls form-controls-data-field">
                                                    <input type="text" class="field field-date" required="" name="field-date" id="field-date" value="" placeholder="احجز تاريخ"
                                                           aria-required="true">
                                                </div>
                                                <!-- /.form-controls -->
                                            </div>
                                            <!-- /.form-row -->

                                            <div class="form-row">
                                                <div class="form-controls">
                                                    <textarea class="textarea" required="" name="field-message" id="field-message" placeholder="الرسالة" aria-required="true"></textarea>
                                                </div>
                                                <!-- /.form-controls -->
                                            </div>
                                            <!-- /.form-row -->

                                            <div class="form-actions">
                                                <input type="submit" value="أحجز موعد" class="button btn-white btn-small">
                                            </div>
                                            <!-- /.form-actions -->
                                        </form>
                                    </div>
                                    <!-- /.from-appointment -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.doctor-body -->
            </div>
            <!-- /.event-box -->
        </article>
    </section>
    <!--end centers -->
    @endsection
