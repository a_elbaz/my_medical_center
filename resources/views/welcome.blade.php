@extends('layouts.app')

@section('content')
    <!-- slider -->
    <div id="owl-demo" class="owl-carousel owl-theme">
        <div class="item">
            <img src="{{asset('images/sliders.jpg')}}" alt="slider">
            <div class="slider">
                <div class="caption">

                </div>
            </div>
        </div>
        <div class="item">
            <img src="{{asset('images/slider-2.jpg')}}" alt="slider">
            <div class="slider">
                <div class="caption">

                </div>
            </div>
        </div>
        <div class="item">
            <img src="images/sliders.jpg" alt="slider">
            <div class="slider">
                <div class="caption">

                </div>
            </div>
        </div>
    </div>
    <!-- end slider -->
    <!-- search -->
    <section class="search">
        <div class="container">
            <div class="row">
                <h1 align="center">البحث عن المركز</h1>
            </div>
            <div class="row">
                <form action="" class="clearfix">
                    <div class="col-xs-12">
                        <input type="text" class="form-control search-input" placeholder="كلمة البحث">
                    </div>
                    <div class="col-xs-12">
                        <div class="btn-form">
                            <button type="submit" class="btn btn-dafult">ابحث الان</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- end search -->
    <!-- service -->
    <section class="about" id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-16">
                    <div class="img-bg">
                        <img class="img-responsive" src="{{asset('images/bg-about.jpg')}}" alt="about">
                    </div>
                </div>
                <div class="col-md-6 col-xs-16">
                    <div class="section-features">
                        <h4>عن الموقع</h4>
                        <h2>خدماتنا</h2>
                        <p>نقدم من خلال موقعنا معلومات عن افضل المراكزالطبية الموجودة تقوم فقط بالبحث عن المركز الطبي الذي تريد
                            ذيارته</p>
                        <ul class="list-features" itemscope="" itemtype="http://schema.org/Product">
                            <li>
                                <p itemprop="name">
                                    <i class="fa fa-plus"></i>
                                    سرعة البحث
                                </p>
                            </li>

                            <li>
                                <p itemprop="name">
                                    <i class="fa fa-plus"></i>
                                    اعداد كبيرة من المراكز الطبية
                                </p>
                            </li>

                            <li>
                                <p itemprop="name">
                                    <i class="fa fa-plus"></i>
                                    ترشيح لافضل المركز
                                </p>
                            </li>
                            <li>
                                <p itemprop="name">
                                    <i class="fa fa-plus"></i>
                                    سرعة البحث
                                </p>
                            </li>

                            <li>
                                <p itemprop="name">
                                    <i class="fa fa-plus"></i>
                                    اعداد كبيرة من المراكز الطبية
                                </p>
                            </li>
                        </ul>
                        <!-- /.list-features -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end service -->
    <!-- contact -->
    <section class="contact" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 align="center">تواصل معانا</h2>
                </div>
                <div class="col-xs-12">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">أكتب بريدك الألكترونى</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">أسم الموضوع</label>
                            <input type="text" class="form-control" id="exampleInputPassword1">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">أكتب استفسارك</label>
                            <textarea class="form-control" name="" id="" cols="30" rows="10"></textarea>
                        </div>
                        <div class="submit-btn">
                            <button type="submit" class="form-control">ارسال</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
