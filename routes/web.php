<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');


Route::get('dr_profile', function (){
    return view('dr_profile');
})->name('dr_profile');

Route::get('centers', function (){
    return view('centers');
})->name('center');

Route::get('center_profile', function (){
    return view('center_profile');
})->name('center_profile');
